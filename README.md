# B-ui

### 介绍

一款非组件化设计的uni-app生态专用的移动端UI框架，一款适合广大开发者轻易上手且能在新老项目中无负担使用的框架。

### 示例体验

平台 | 链接 | 二维码
-- | -- | -- 
H5 | [预览链接](https://mydarling.gitee.io/b-ui "H5版在线体验") | <img width="200" src="https://mydarling.gitee.io/b-ui/_/h5.png">

### 框架文档

#### <font color="red">文档还在编写中，请先下载示例项目后参考示例项目使用</font>

### 框架下载&安装

#### 下载：

1. 前往 uni-app 下载市场：[https://ext.dcloud.net.cn/plugin?id=10582](https://ext.dcloud.net.cn/plugin?id=10582)
2. 选择 使用 HBuilderX 导入插件 或 下载插件ZIP
3. 如果是选择 HBuilderX 导入插件 则会自动安装，如果是选择 下载插件ZIP 把解压后的文件移入到 `根目录/uni_modules/b-ui` 目录中

#### 安装：

1. 安装CSS： `App.vue` 文件中添加如下代码

```html
<style lang="scss">
	/*每个页面公共css */
	@import "@/uni_modules/b-ui/css/main.bundle.scss";
</style>
```

2. 安装JS： `main.js` 文件中添加如下代码

```javascript
// 在 main.js 的最后面添加如下文件
require("@/uni_modules/b-ui/js/main.bundle.js");
```

### 兼容说明

#### Vue版本兼容

Vue2 | Vue3
-- | -- 
√ | 未测试

#### 平台兼容

H5 | APP-Vue | 微信小程序 | APP-NVue | 其他小程序 | 快应用 | PC
-- | -- | -- | --  | --  | --  | -- 
√ | √ | √ | × | 理论兼容 | 懒得测试 | 不推荐使用

### 依赖说明

- scss/sass，使用 `HBuilder X` 通过 菜单 “工具” > “插件安装” > “scss/sass编译” 进行安装

### B-ui 名字的由来

- 【Best 合适的】，这款UI是一个适合广大开发者轻易上手的。
- 【Basic 基础的】，这款UI只提供基础的CSS布局和常用的轻型组件封装，对新老项目都能无负担的使用。
- 【Beautiful 美丽的】，这款UI是美丽且符合大众审美的。

### 关于作者
- 河浪
- helang.love@qq.com

### 开源协议
- B-ui 遵循[MIT](https://en.wikipedia.org/wiki/MIT_License)开源协议，意味着您无需支付任何费用，也无需授权，即可将 B-ui 应用到您的产品中。
- 注意：这并不意味着您可以将 B-ui 应用到非法的领域，比如涉及赌博，暴力等方面。如因此产生纠纷或法律问题，B-ui 不承担任何责任。