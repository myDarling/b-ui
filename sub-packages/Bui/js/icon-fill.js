/*
 * B-ui 实底图标配置文件
*/

export default [
	"eyeclose-fill","eye-fill","check-circle-fill","info-circle-fill",
	"left-circle-fill","down-circle-fill","up-circle-fill","right-circle-fill",
	"plus-circle-fill","minus-circle-fill","close-circle-fill","question-circle-fill",
	"compass-fill","Dollar-circle-fill","play-circle-fill","poweroff-circle-fill",
	"frown-fill","meh-fill","smile-fill","warning-circle-fill",
	"stop-fill","time-circle-fill","YUAN-circle-fill","heart-fill",
	"piechart-circle-fil","dashboard-fill","message-fill","calculator-fill",
	"control-fill","layout-fill","appstore-fill","mobile-fill",
	"redenvelope-fill","safetycertificate-f","insurance-fill","file-markdown-fill",
	"file-text-fill","file-ppt-fill","file-word-fill","file-zip-fill",
	"file-pdf-fill","file-image-fill","file-copy-fill","folder-add-fill",
	"folder-fill","folder-open-fill","calendar-check-fill","image-fill",
	"idcard-fill","fund-fill","read-fill","delete-fill",
	"notification-fill","flag-fill","moneycollect-fill","rest-fill",
	"shopping-fill","skin-fill","video-fill","sound-fill",
	"bulb-fill","bell-fill","filter-fill","fire-fill",
	"funnelplot-fill","hourglass-fill","gift-fill","home-fill",
	"trophy-fill","location-fill","cloud-fill","customerservice-fill",
	"like-fill","unlike-fill","lock-fill","unlock-fill",
	"star-fill","alert-fill","api-fill","highlight-fill",
	"phone-fill","edit-fill","pushpin-fill","rocket-fill",
	"thunderbolt-fill","tag-fill","wrench-fill","tags-fill",
	"bank-fill","camera-fill","error-fill","crown-fill",
	"mail-fill","car-fill","printer-fill","shop-fill",
	"setting-fill","USB-fill","IE","dingtalk",
	"android-fill","apple-fill","HTML-fill","windows-fill",
	"QQ","weibo","wechat-fill","chrome-fill",
	"alipay-circle-fill","aliwangwang-fill","github-fill","QQ-circle-fill",
	"IE-circle-fill","dingtalk-circle-fill","taobao-circle-fill","weibo-circle-fill",
	"zhihu-circle-fill","alipay-square-fill","dingtalk-square-fill","IE-square-fill",
	"QQ-square-fill","taobao-square-fill","weibo-square-fill","zhihu-square-fill",
	"audio-fill","bug-fill","signal-fill"
]