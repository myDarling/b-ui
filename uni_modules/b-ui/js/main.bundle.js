/*
 * B-ui JS依赖打包文件
 * 将 B-ui 所有的JS依赖全部集成
*/

let version = "1.0.1";

import Utils from "./utils.js";
import UtilsDate from "./utils-date.js";


uni.Bui = {
	version,
	...Utils,
	...UtilsDate
}