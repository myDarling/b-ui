/*
 * B-ui 日期工具
*/

export default {
	// 日期对象
	DateObject(val = "") {
		var params = val || "";

		/* 无有效参数返回当前时间 */
		if (!params) {
			return new Date();
		}
		/* 日期对象 */
		if (typeof params == "object") {
			return params;
		}
		/* 数值 */
		if (typeof params == "number") {
			return new Date(params);
		}

		/* 字符串 */
		if (typeof params == "string") {
			/* 尝试转为数值 */
			var temp = Number(params);
			if (temp && temp > 0) {
				return new Date(temp);
			}
			/* 检测是否是一个有效的日期字符串 */
			if (/\d{4}[-\/]\d{1,2}[-\/]\d{1,2}/.test(params)) {
				/* - 替换为 / 符号，兼容 iOS 系统 */
				params = params.replace(/-/g, "/");
			} else {
				return new Date();
			}
			/* 清除两端空格 */
			params = params.trim();
			/* 检测是否存在时分秒内容，无则自动补齐 */
			if (!/\d{1,2}:\d{1,2}:\d{1,2}/.test(params)) {
				params += " 00:00:00";
			}
			return new Date(params);
		}
	},
	// 日期JSON
	DateJSON(val) {
		var dateObj = this.DateObject(val);

		var year = dateObj.getFullYear();
		var month = dateObj.getMonth() + 1;
		var day = dateObj.getDate();
		var hour = dateObj.getHours();
		var minute = dateObj.getMinutes();
		var second = dateObj.getSeconds();

		var join = function(num) {
			if (num < 10) {
				return "0" + num;
			} else {
				return num;
			}
		};

		return {
			year: year,
			month: join(month),
			day: join(day),
			hour: join(hour),
			minute: join(minute),
			second: join(second),
		};
	},
	// 日期格式化
	DateFormat(val, format) {
		var format = format || "yyyy-MM-dd hh:mm:ss";
		var dateJson = this.DateJSON(val);

		var formatArr = [{
				key: "yyyy",
				value: dateJson.year
			},
			{
				key: "MM",
				value: dateJson.month
			},
			{
				key: "dd",
				value: dateJson.day
			},
			{
				key: "hh",
				value: dateJson.hour
			},
			{
				key: "mm",
				value: dateJson.minute
			},
			{
				key: "ss",
				value: dateJson.second
			},
		];

		formatArr.forEach(function(item) {
			var reg = new RegExp(item.key);
			format = format.replace(reg, item.value);
		});

		return format;
	},
	// 日期转时间辍
	DateToNumber(val) {
		var dateObj = this.DateObject(val);
		return dateObj.getTime();
	}
}
