/*
 * B-ui 工具库
*/

export default {
	// 节流，在规定时间内连续触发时只有最后一次触发生效
	throttle(func,params = {}){
		// 定义节流计时器名称，可设置多个名称，让多个节流函数互不干扰
		let timeName = `_throttleTime${params.name || ''}`;
		// Bui 全局对象存在节流计时器则中断函数执行
		if(uni.Bui[timeName]){
			return;
		}
		// 默认延迟事件 0.6秒
		let wait = params.wait || 600;
		
		// 延迟时间后清除节流计时器
		uni.Bui[timeName] = setTimeout(()=>{
			clearTimeout(uni.Bui[timeName]);
			delete uni.Bui[timeName];
		},wait);
		
		// 执行回调函数
		func && func();
	},
	// 防抖，规定时间内，只触发一次。触发后需要在等待时间结束后才能第二次执行
	debounce(func,params = {}){
		// 定义防抖计时器名称，可设置多个名称，让多个防抖函数互不干扰
		let timeName = `_debounceTime${params.name || ''}`;
		
		// 默认延迟事件 0.6秒
		let wait = params.wait || 600;
		
		// 清除历史未执行的防抖计时器
		clearTimeout(uni.Bui[timeName]);
		
		// 执行回调函数
		uni.Bui[timeName] = setTimeout(()=>{
			func && func();
		},wait);
	}
}